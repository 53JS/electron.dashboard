'use strict';

var electron = require('electron');
var eApp = electron.app;
var BrowserWindow = electron.BrowserWindow;
var path = require('path')
var url = require('url')
var express = require('express')
var app = express();

app.use('/', express.static('./node_modules/admin-lte/'));
app.listen(3000);
console.log('ready');


let mainWindow = null;

let createWindow = function createWindow() {
	/* 
		Check the API for more options 
		https://github.com/electron/electron/blob/master/docs/api/browser-window.md
	*/
	mainWindow = new BrowserWindow({
		fullscreen: false,
		center: true,
		alwaysOnTop: false,
		autoHideMenuBar: true,
		useContentSize: true,
		webPreferences : {
			nodeIntegration: false
		}
	});


	mainWindow.loadURL('http://localhost:3000');
	// Open the DevTools.
	//mainWindow.openDevTools();


	mainWindow.on('closed', function onClosed() {
		mainWindow = null;
	});
}

// Quit when all windows are closed.
eApp.on('window-all-closed', function onWindowAllClosed() {
	eApp.quit();
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
eApp.on('ready', createWindow)

eApp.on('activate', function() {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow()
	}
});